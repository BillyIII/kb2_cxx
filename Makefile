
CARGO ?= cargo
CXXBRIDGE ?= $(HOME)/.cargo/bin/cxxbridge

DEBUGLIB = target/debug/libkb2_cxx.a
HPP = target/kb2.hpp
CPP = target/kb2.cpp

all: debug

debug: $(DEBUGLIB) ffi

$(DEBUGLIB):
	$(CARGO) +nightly build

ffi: $(HPP) $(CPP)

$(HPP):
	$(CXXBRIDGE) src/ffi.rs --header -o $@

$(CPP):
	$(CXXBRIDGE) src/ffi.rs -o $@

clean:
	rm -f $(DEBUGLIB) $(HPP) $(CPP)

clean-all:
	$(CARGO) clean
