
use crate::types::{ Result };


pub struct Runtime(pub tokio::runtime::Runtime);

pub fn runtime_create() -> Result<Box<Runtime>> {
    Ok(Box::new(Runtime(tokio::runtime::Runtime::new()?)))
}
