
use crate::types::{ Result, DbId };
use crate::db::{ Db };
use crate::runtime::{ Runtime };


pub struct FilesItem(pub kb2::backends::files::FilesItem);

impl FilesItem {
    pub fn get_id(&self) -> DbId {
        self.0.get_id()
    }
        
    pub fn get_path(&self) -> String {
        self.0.path.clone()
    }
    
    pub fn set_path(&mut self, value: &str) -> () {
        self.0.path = value.to_owned();
    }
        
    pub fn get_mime(&self) -> String {
        self.0.mime.clone().unwrap_or("".to_owned())
    }
    
    pub fn set_mime(&mut self, value: &str) -> () {
        self.0.mime = match value { "" => None, m => Some(m.to_owned()) };
    }
    
    pub fn save(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()> {
        let s = self.0.clone();
        rt.0.block_on(db.0.exec(move |conn| {
            s.save(conn)
        }))?
    }
}

pub fn load_file_item(rt: &Box<Runtime>, db: &Box<Db>, id: DbId) -> Result<Box<FilesItem>> {
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(FilesItem(kb2::backends::files::FilesItem::load(conn, id)?)))
    }))?
}
