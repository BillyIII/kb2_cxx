
use crate::types::{ Result, DbId };
use crate::db::{ Db };
use crate::runtime::{ Runtime };


pub struct FilesCollection(pub kb2::backends::files::FilesCollection);

impl FilesCollection {
    pub fn get_id(&self) -> DbId {
        self.0.get_id()
    }
        
    pub fn get_path(&self) -> String {
        self.0.path.clone()
    }
    
    pub fn set_path(&mut self, value: &str) -> () {
        self.0.path = value.to_owned();
    }
    
    pub fn get_max_text_size(&self) -> i64 {
        self.0.max_text_size.unwrap_or(0)
    }
    
    pub fn set_max_text_size(&mut self, value: i64) -> () {
        self.0.max_text_size = if value <= 0 { None } else { Some(value) };
    }
    
    pub fn get_max_content_size(&self) -> i64 {
        self.0.max_content_size.unwrap_or(0)
    }
    
    pub fn set_max_content_size(&mut self, value: i64) -> () {
        self.0.max_content_size = if value <= 0 { None } else { Some(value) };
    }
    
    pub fn delete_(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()> {
        let s = self.0.clone();
        rt.0.block_on(db.0.exec(move |conn| {
            s.delete(conn)
        }))?
    }
    
    pub fn save(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()> {
        let s = self.0.clone();
        rt.0.block_on(db.0.exec(move |conn| {
            s.save(conn)
        }))?
    }
}

pub fn load_file_collection(rt: &Box<Runtime>, db: &Box<Db>, id: DbId) -> Result<Box<FilesCollection>> {
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(FilesCollection(kb2::backends::files::FilesCollection::load(conn, id)?)))
    }))?
}
