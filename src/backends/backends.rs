
use std::collections::{ HashMap };

use crate::types::{ Result, DbId };
use crate::collections::{ Collection, Tags };
use crate::db::{ Db };
use crate::runtime::{ Runtime };


pub struct CollectionBackends {
    backends: HashMap<String, Box<dyn kb2::backends::CollectionBackend>>,
}

impl CollectionBackends {    
    pub fn load() -> Result<Box<CollectionBackends>> {
        let mut backends = HashMap::<String, Box<dyn kb2::backends::CollectionBackend>>::new();
        backends.insert(kb2::backends::files::FILES_COLLECTION_BACKEND_CODE.to_owned(),
                        Box::new(kb2::backends::files::FilesCollectionBackend::new()));

        Ok(Box::new(CollectionBackends { backends }))
    }

    fn get(&self, backend: &str) -> Result<&Box<dyn kb2::backends::CollectionBackend>> {
        self.backends.get(backend).ok_or_else(|| format_err!("Invalid backend: {}", backend))
    }

    pub fn backend_description(&self, backend: &str) -> Result<String> {
        Ok(self.get(backend)?.backend_description())
    }
    
    pub fn collection_summary(&self, rt: &Box<Runtime>, db: &Box<Db>, coll: &Box<Collection>) -> Result<String> {
        rt.0.block_on(
            self.get(coll.0.get_backend())?.collection_summary(&db.0, coll.0.get_id())
        )
    }
    
    pub fn update_collection(&self, rt: &Box<Runtime>, db: &Box<Db>, tags: &Box<Tags>, coll: &Box<Collection>)
                             -> Result<()> {
        rt.0.block_on(
            self.get(coll.0.get_backend())?.update_collection(&db.0, tags.0.clone(), coll.0.get_id())
        )
    }
    
    pub fn delete_collection(&self, rt: &Box<Runtime>, db: &Box<Db>, coll: &Box<Collection>) -> Result<()> {
        rt.0.block_on(
            self.get(coll.0.get_backend())?.delete_collection(&db.0, coll.0.get_id())
        )
    }
    
    pub fn item_file(&self, rt: &Box<Runtime>, db: &Box<Db>, id: DbId) -> Result<kb2::backends::CollectionItemFile> {
        rt.0.block_on(async {
            let backend = db.0.exec(move |conn| -> Result<_> {
                let item = kb2::collections::CollectionItem::load(conn, id)?;
                let coll = kb2::collections::Collection::load(conn, item.get_collection())?;
                Ok(coll.get_backend().to_owned())
            }).await??;
            self.get(&backend)?.item_file(&db.0, id).await
        })
    }
}

pub fn load_collection_backends() -> Result<Box<CollectionBackends>> {
    CollectionBackends::load()
}
