
use crate::types::{ Result };


pub struct Db(pub kb2::db::Db);

pub fn db_create(uri: &str) -> Result<Box<Db>> {
    Ok(Box::new(Db(kb2::db::Db::new(kb2::db::connect(uri)?))))
}
