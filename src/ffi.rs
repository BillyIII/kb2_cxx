
use std::path::{ PathBuf };
use kb2::utils::{ mailcap };

use crate::types::*;
use crate::db::*;
use crate::runtime::*;
use crate::collections::*;
use crate::backends::*;
use crate::backends::files::*;


#[cxx::bridge(namespace = "kb2")]
mod ffi {
    extern "Rust" {
        type Db;
        
        fn db_create(uri: &str) -> Result<Box<Db>>;
    }
    impl Box<Db> {}
    
    extern "Rust" {
        type Runtime;
        
        fn runtime_create() -> Result<Box<Runtime>>;
    }
    impl Box<Runtime> {}
    
    extern "Rust" {
        type Collection;

        fn get_id(&self) -> i32;
        fn get_backend(&self) -> String;
        fn get_name(&self) -> String;
        fn set_name(&mut self, value: &str) -> ();
        
        fn delete_(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()>;
        fn save(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()>;
            
        fn create_collection(rt: &Box<Runtime>, db: &Box<Db>, backend: &str) -> Result<Box<Collection>>;
        fn load_collection(rt: &Box<Runtime>, db: &Box<Db>, id: i32) -> Result<Box<Collection>>;
        // Unsupported type in Vec.
        // fn load_all_collections(rt: &Box<Runtime>, db: &Box<Db>) -> Result<Vec<Box<Collection>>>;
        fn get_all_collections(rt: &Box<Runtime>, db: &Box<Db>) -> Result<Vec<i32>>;
    }
    impl Box<Collection> {}
    
    extern "Rust" {
        type CollectionItem;

        fn get_id(&self) -> i32;
        fn get_collection(&self) -> i32;
        fn get_title(&self) -> String;
        fn get_body(&self) -> String;
        fn get_mtime(&self) -> i64;
        fn get_tags(&self) -> Vec<i32>;
            
        fn load_collection_item(rt: &Box<Runtime>, db: &Box<Db>, id: i32) -> Result<Box<CollectionItem>>;
        fn load_collection_item_preview(rt: &Box<Runtime>, db: &Box<Db>, id: i32, body_size: i64) -> Result<Box<CollectionItem>>;
        fn load_collection_item_body(rt: &Box<Runtime>, db: &Box<Db>, id: i32) -> Result<String>;
    }
    impl Box<CollectionItem> {}
    
    pub struct TagAndName {
        id: i32,
        name: String,
    }

    extern "Rust" {
        type Tags;

        fn get_id(&self, tag: &str) -> Result<i32>;
        fn get_tag(&self, id: i32) -> Result<String>;
        // fn get_all(&self) -> Vec<(DbId, String)>;
            
        fn load_tags(rt: &Box<Runtime>, db: &Box<Db>) -> Result<Box<Tags>>;
        fn get_all_tags(tags: &Box<Tags>) -> Result<Vec<TagAndName>>;
    }
    impl Box<Tags> {}

    pub struct CollectionItemFile {
        pub mime: String,
        pub uri: String,
    }

    extern "Rust" {
        type CollectionBackends;

        fn backend_description(&self, backend: &str) -> Result<String>;
        fn collection_summary(&self, rt: &Box<Runtime>, db: &Box<Db>, coll: &Box<Collection>) -> Result<String>;
        fn update_collection(&self, rt: &Box<Runtime>, db: &Box<Db>, tags: &mut Box<Tags>, coll: &Box<Collection>) -> Result<()>;
        fn delete_collection(&self, rt: &Box<Runtime>, db: &Box<Db>, coll: &Box<Collection>) -> Result<()>;
        
        fn load_collection_backends() -> Result<Box<CollectionBackends>>;
        fn collection_backend_item_file(backends: &Box<CollectionBackends>, rt: &Box<Runtime>, db: &Box<Db>, id: i32)
                                        -> Result<CollectionItemFile>;
    }
    impl Box<CollectionBackends> {}

    pub enum Sorting {
        None,
        RankAsc, RankDesc,
        DateAsc, DateDesc,
    }

    extern "Rust" {
        fn find_items(rt: &Box<Runtime>, db: &Box<Db>, tags: &Box<Tags>, limit: i64, sort: Sorting, query: &str)
                      -> Result<Vec<i32>>;
    }

    pub struct FilesCollectionPair {
        pub coll: Box<Collection>,
        pub fcoll: Box<FilesCollection>,
    }

    extern "Rust" {
        type FilesCollection;

        fn get_id(&self) -> i32;

        fn get_path(&self) -> String;
        fn set_path(&mut self, value: &str) -> ();
        fn get_max_text_size(&self) -> i64;
        fn set_max_text_size(&mut self, value: i64) -> ();
        fn get_max_content_size(&self) -> i64;
        fn set_max_content_size(&mut self, value: i64) -> ();
        
        fn delete_(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()>;
        fn save(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()>;
            
        fn create_file_collection(rt: &Box<Runtime>, db: &Box<Db>) -> Result<FilesCollectionPair>;
        fn load_file_collection(rt: &Box<Runtime>, db: &Box<Db>, id: i32) -> Result<Box<FilesCollection>>;
    }
    impl Box<FilesCollection> {}

    pub struct FilesItemPair {
        pub item: Box<CollectionItem>,
        pub fitem: Box<FilesItem>,
    }
    
    extern "Rust" {
        type FilesItem;

        fn get_id(&self) -> i32;

        fn get_path(&self) -> String;
        fn set_path(&mut self, value: &str) -> ();
        fn get_mime(&self) -> String;
        fn set_mime(&mut self, value: &str) -> ();
        
        fn save(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()>;
            
        fn create_file_item(rt: &Box<Runtime>, db: &Box<Db>, coll: i32) -> Result<FilesItemPair>;
        fn load_file_item(rt: &Box<Runtime>, db: &Box<Db>, id: i32) -> Result<Box<FilesItem>>;
    }
    impl Box<FilesItem> {}

    pub enum MailcapEntryMode {
        Gui,
        Terminal,
    }
    
    extern "Rust" {
        fn open_with_mailcap(rt: &Box<Runtime>, mime: &str, file: &str, mode: MailcapEntryMode) -> Result<()>;
    }
}

pub fn create_file_collection(rt: &Box<Runtime>, db: &Box<Db>) -> Result<ffi::FilesCollectionPair> {
    let (coll, fcoll) = rt.0.block_on(db.0.exec(move |conn| kb2::backends::files::FilesCollection::create(conn)))??;
    Ok(ffi::FilesCollectionPair {
        coll: Box::new(Collection(coll)),
        fcoll: Box::new(FilesCollection(fcoll)),
    })
}

pub fn create_file_item(rt: &Box<Runtime>, db: &Box<Db>, coll: i32) -> Result<ffi::FilesItemPair> {
    let (item, fitem) = rt.0.block_on(db.0.exec(move |conn| kb2::backends::files::FilesItem::create(conn, coll)))??;
    Ok(ffi::FilesItemPair {
        item: Box::new(CollectionItem(item)),
        fitem: Box::new(FilesItem(fitem)),
    })
}

fn get_all_tags(tags: &Box<Tags>) -> Result<Vec<ffi::TagAndName>> {
    Ok(tags.get_all()?.drain(..).map(|(i,n)| ffi::TagAndName { id: i, name: n.clone() }).collect())
}

fn collection_backend_item_file(backends: &Box<CollectionBackends>, rt: &Box<Runtime>, db: &Box<Db>, id: DbId)
                                -> Result<ffi::CollectionItemFile> {
    let res = backends.item_file(rt, db, id)?;

    Ok(ffi::CollectionItemFile {
        mime: res.mime.unwrap_or("".to_owned()),
        uri: res.uri,
    })
}

fn find_items(rt: &Box<Runtime>, db: &Box<Db>, tags: &Box<Tags>, limit: i64, sort: ffi::Sorting, query: &str)
              -> Result<Vec<DbId>> {
    let limit = if limit > 0 {
        Some(limit)
    }
    else {
        None
    };

    let sort = match sort {
        ffi::Sorting::None => None,
        ffi::Sorting::RankAsc => Some(kb2::search::Sorting {
            field: kb2::search::SortingField::Rank,
            dir: kb2::search::SortingDirection::Asc,
        }),
        ffi::Sorting::RankDesc => Some(kb2::search::Sorting {
            field: kb2::search::SortingField::Rank,
            dir: kb2::search::SortingDirection::Desc,
        }),
        ffi::Sorting::DateAsc => Some(kb2::search::Sorting {
            field: kb2::search::SortingField::Date,
            dir: kb2::search::SortingDirection::Asc,
        }),
        ffi::Sorting::DateDesc => Some(kb2::search::Sorting {
            field: kb2::search::SortingField::Date,
            dir: kb2::search::SortingDirection::Desc,
        }),
        _ => None,
    };

    let t = tags.0.clone();
    let q = query.to_owned();
    rt.0.block_on(db.0.exec(move |conn| {
        kb2::search::find_items(conn, t, limit, sort, &q)
    }))?
}

pub fn open_with_mailcap(rt: &Box<Runtime>, mime: &str, file: &str, mode: ffi::MailcapEntryMode) -> Result<()>
{
    let mode = match mode {
        ffi::MailcapEntryMode::Gui => Ok(mailcap::MailcapEntryMode::Gui),
        ffi::MailcapEntryMode::Terminal => Ok(mailcap::MailcapEntryMode::Terminal),
        _ => Err(format_err!("Invalid mailcap entry mode")),
    }?;

    let mime = mime.parse()?;
    let file : PathBuf = file.parse()?;
    
    rt.0.block_on(mailcap::open_with_mailcap(&mime, &file, mode))
}
