
use crate::types::{ Result, DbId };
use crate::db::{ Db };
use crate::runtime::{ Runtime };


pub struct CollectionItem(pub kb2::collections::CollectionItem);

impl CollectionItem {
    pub fn get_id(&self) -> DbId {
        self.0.get_id()
    }
        
    pub fn get_collection(&self) -> DbId {
        self.0.get_collection()
    }
        
    pub fn get_title(&self) -> String {
        self.0.title.clone()
    }
        
    pub fn get_body(&self) -> String {
        self.0.body.clone()
    }
        
    pub fn get_mtime(&self) -> i64 {
        self.0.mtime
    }
    
    pub fn get_tags(&self) -> Vec<DbId> {
        self.0.tags.iter().map(|v| *v).collect()
    }
}

pub fn load_collection_item(rt: &Box<Runtime>, db: &Box<Db>, id: DbId) -> Result<Box<CollectionItem>> {
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(CollectionItem(kb2::collections::CollectionItem::load(conn, id)?)))
    }))?
}

pub fn load_collection_item_preview(rt: &Box<Runtime>, db: &Box<Db>, id: DbId, body_size: i64)
                                    -> Result<Box<CollectionItem>> {
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(CollectionItem(kb2::collections::CollectionItem::load_preview(conn, id, body_size)?)))
    }))?
}

pub fn load_collection_item_body(rt: &Box<Runtime>, db: &Box<Db>, id: DbId) -> Result<String> {
    rt.0.block_on(db.0.exec(move |conn| {
        kb2::collections::CollectionItem::load_body(conn, id)
    }))?
}
