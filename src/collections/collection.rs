
use crate::types::{ Result, DbId };
use crate::db::{ Db };
use crate::runtime::{ Runtime };


pub struct Collection(pub kb2::collections::Collection);

impl Collection {
    pub fn get_id(&self) -> DbId {
        self.0.get_id()
    }
        
    pub fn get_backend(&self) -> String {
        self.0.get_backend().to_owned()
    }
    
    pub fn get_name(&self) -> String {
        self.0.name.clone()
    }
    
    pub fn set_name(&mut self, value: &str) -> () {
        self.0.name = value.to_owned();
    }
    
    pub fn delete_(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()> {
        let s = self.0.clone();
        rt.0.block_on(db.0.exec(move |conn| {
            s.delete(conn)
        }))?
    }
    
    pub fn save(&self, rt: &Box<Runtime>, db: &Box<Db>) -> Result<()> {
        let s = self.0.clone();
        rt.0.block_on(db.0.exec(move |conn| {
            s.save(conn)
        }))?
    }
}

pub fn create_collection(rt: &Box<Runtime>, db: &Box<Db>, backend: &str) -> Result<Box<Collection>> {
    let b = backend.to_owned();
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(Collection(kb2::collections::Collection::create(conn, &b)?)))
    }))?
}

pub fn load_collection(rt: &Box<Runtime>, db: &Box<Db>, id: DbId) -> Result<Box<Collection>> {
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(Collection(kb2::collections::Collection::load(conn, id)?)))
    }))?
}

pub fn get_all_collections(rt: &Box<Runtime>, db: &Box<Db>) -> Result<Vec<DbId>> {
    rt.0.block_on(db.0.exec(move |conn| {
        kb2::collections::Collection::get_all(conn)
    }))?
}
