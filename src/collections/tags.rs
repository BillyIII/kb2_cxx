
use crate::types::{ Result, DbId, Amrc };
use crate::db::{ Db };
use crate::runtime::{ Runtime };


pub struct Tags(pub Amrc<kb2::collections::Tags>);

impl Tags {
    pub fn get_id(&self, tag: &str) -> Result<DbId> {
        self.0.lock("Tags::get_id")?.borrow().get_id(tag).ok_or_else(|| format_err!("Tag not found: {}", tag))
    }
    
    pub fn get_tag(&self, id: DbId) -> Result<String> {
        self.0.lock("Tags::get_tag")?.borrow().get_tag(id).ok_or_else(|| format_err!("Tag not found: {}", id))
    }
    
    pub fn get_all(&self) -> Result<Vec<(DbId, String)>> {
        Ok(self.0.lock("Tags::get_all")?.borrow().iter().map(|(id, name)| (*id, name.clone())).collect())
    }
}

pub fn load_tags(rt: &Box<Runtime>, db: &Box<Db>) -> Result<Box<Tags>> {
    rt.0.block_on(db.0.exec(move |conn| {
        Ok(Box::new(Tags(Amrc::new(kb2::collections::Tags::load(conn)?))))
    }))?
}
